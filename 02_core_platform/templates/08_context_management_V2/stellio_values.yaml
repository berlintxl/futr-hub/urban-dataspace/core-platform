# Default values for stellio context broker
global:
  imageRegistry: "{{ cm_stellio.image_registry }}"
  usePostgresOperator: {{ inv_cm.stellio.use_postgres_operator }}

nameOverride: ""
fullnameOverride: ""

defaultPodSecurityContext: &defaultPodSecurityContext
  runAsNonRoot: true
  runAsUser: 1000
  runAsGroup: 1000

defaultSecurityContext: &defaultSecurityContext
  allowPrivilegeEscalation: false
  capabilities:
    drop:
    - ALL
  privileged: false
  readOnlyRootFilesystem: true

api_gateway:
  replicaCount: "{{ inv_replicas.cm.api_gateway }}"

  ingress:
    enabled: false
    className: ""
    annotations:
      kubernetes.io/ingress.class: nginx
      kubernetes.io/tls-acme: "true"
    host: null
    existingCertificate: ""

  image:
    registry: ""
    pullPolicy: IfNotPresent
    pullSecrets: []
    repository: "{{ cm_stellio.api_gateway.image_repository }}"
    tag: "{{ cm_stellio.api_gateway.image_tag }}"
    digest: ""

  podAnnotations: {}

  podSecurityContext: *defaultPodSecurityContext

  resources:
    # We usually recommend not to specify default resources and to leave this as a conscious
    # choice for the user. This also increases chances charts run on environments with little
    # resources, such as Minikube. If you do want to specify resources, uncomment the following
    # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
    limits:
      cpu: 500m
      memory: 1500Mi
    requests:
      cpu: 100m
      memory: 500Mi

  securityContext: *defaultSecurityContext

kafka:
  replicaCount: "{{ inv_replicas.cm.kafka }}"
  image:
    registry: ""
    pullPolicy: IfNotPresent
    pullSecrets: []
    repository: "{{ cm_stellio.kafka.image_repository }}"
    tag: "{{ cm_stellio.kafka.image_tag }}"
    digest: ""

  persistence:
    accessMode: ReadWriteOnce
    storageClass: "{{ inv_k8s.storage_class.loc }}"
    size: 100Mi

  podAnnotations: {}

  podSecurityContext: {}
    # fsGroup: 2000
  resources:
    # We usually recommend not to specify default resources and to leave this as a conscious
    # choice for the user. This also increases chances charts run on environments with little
    # resources, such as Minikube. If you do want to specify resources, uncomment the following
    # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
    limits:
      cpu: 1.0
      memory: 1500Mi
    requests:
      cpu: 100m
      memory: 100Mi

  securityContext: {}
    # capabilities:
    #   drop:
    #   - ALL
    # readOnlyRootFilesystem: true
    # runAsNonRoot: true
    # runAsUser: 1000

postgres:
  replicaCount: {{ inv_replicas.cm.postgresql }}
  credentials:
    username: {{ inv_cm.stellio.postgres_credentials.username }}
    password: {{ inv_cm.stellio.postgres_credentials.password }}
  image:
    registry: ""
    pullPolicy: IfNotPresent
    pullSecrets: []
    repository: stellio/stellio-timescale-postgis
    tag: 14-2.9.1-3.3
    digest: ""
  persistence:
    accessMode: ReadWriteOnce
    storageClass: {{ inv_k8s.storage_class.loc }}
    size: 50Gi
  podAnnotations: {}

  podSecurityContext: {}
    # fsGroup: 2000

  resources:
    # We usually recommend not to specify default resources and to leave this as a conscious
    # choice for the user. This also increases chances charts run on environments with little
    # resources, such as Minikube. If you do want to specify resources, uncomment the following
    # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
    limits:
      cpu: 1.0
      memory: 1Gi
    requests:
      cpu: 300m
      memory: 300Mi

  securityContext: {}
    # capabilities:
    #   drop:
    #   - ALL
    # readOnlyRootFilesystem: true
    # runAsNonRoot: true
    # runAsUser: 1000

search:

  postgresHost: "{{ cm_stellio_settings.cluster_team }}.{{ k8s_namespace }}.svc.cluster.local"
  postgresCredentialsSecret: "stellio-search-owner-user.{{ cm_stellio_settings.cluster_team }}.credentials.postgresql.acid.zalan.do"

  replicaCount: "{{ inv_replicas.cm.search_service }}"
  image:
    registry: ""
    pullPolicy: IfNotPresent
    pullSecrets: []
    repository: "{{ cm_stellio.search_service.image_repository }}"
    tag: "{{ cm_stellio.search_service.image_tag }}"
    digest: ""
  podAnnotations: {}

  podSecurityContext: *defaultPodSecurityContext

  resources:
    # We usually recommend not to specify default resources and to leave this as a conscious
    # choice for the user. This also increases chances charts run on environments with little
    # resources, such as Minikube. If you do want to specify resources, uncomment the following
    # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
    limits:
      cpu: 1.0
      memory: 1500Mi
    requests:
      cpu: 100m
      memory: 500Mi

  securityContext: *defaultSecurityContext

subscription:

  postgresHost: "{{ cm_stellio_settings.cluster_team }}.{{ k8s_namespace }}.svc.cluster.local"
  postgresCredentialsSecret: "stellio-subscription-owner-user.{{ cm_stellio_settings.cluster_team }}.credentials.postgresql.acid.zalan.do"

  replicaCount: "{{ inv_replicas.cm.subscription_service }}"
  image:
    registry: ""
    pullPolicy: IfNotPresent
    pullSecrets: []
    repository: "{{ cm_stellio.subscription_service.image_repository }}"
    tag: "{{ cm_stellio.subscription_service.image_tag }}"
    digest: ""
  podAnnotations: {}

  podSecurityContext: *defaultPodSecurityContext

  resources:
    # We usually recommend not to specify default resources and to leave this as a conscious
    # choice for the user. This also increases chances charts run on environments with little
    # resources, such as Minikube. If you do want to specify resources, uncomment the following
    # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
    limits:
      cpu: 1.0
      memory: 1500Mi
    requests:
      cpu: 100m
      memory: 500Mi

  securityContext: *defaultSecurityContext
