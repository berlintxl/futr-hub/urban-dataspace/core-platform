- name: Set default facts for K8S
  ansible.builtin.set_fact:
    k8s_config: "{{ K8S_KUBECONFIG_PATH }}/{{ inv_am.apisix.ns_kubeconfig }}"
    k8s_context: "{{ inv_k8s.config.context }}"
    k8s_namespace: "{{ inv_am.apisix.ns_name | lower }}"

- name: "Create namespace {{ k8s_namespace }}"
  kubernetes.core.k8s:
    state: present
    kubeconfig: "{{ k8s_config }}"
    context: "{{ k8s_context }}"
    definition:
      api_version: v1
      kind: Namespace
      metadata:
        name: "{{ k8s_namespace }}"
        labels:
          name: "{{ k8s_namespace }}"
  when: inv_am.apisix.ns_create == "true"

- name: Deploy APISiX with Dashboard and Ingress Controller using Helm Chart 
  include_tasks: ansible-templates/k8s_tasks/k8s-helm.yml
  vars:
    helm_repo_name: "{{ api_management.helm_repo_name }}"
    helm_repo_url: "{{ api_management.helm_repo_url }}"
    helm_chart_name: "{{ api_management.helm_chart_name }}"
    helm_release_name: "{{ api_management.helm_release_name }}"
    helm_chart_version: "{{ api_management.helm_chart_version }}"
    helm_chart_reference: "{{ helm_repo_name }}/{{ helm_chart_name }}"
    helm_values_file: "10_api_management/deployment/apisix-values.yaml"
    helm_create_namespace: "{{ inv_am.apisix.ns_create }}"

- name: Wait for APISiX to be ready
  kubernetes.core.k8s_info:
    kind: Pod
    namespace: "{{ k8s_namespace }}"
    kubeconfig: "{{ k8s_config }}"
    context: "{{ k8s_context }}"
    label_selectors: "app.kubernetes.io/name=apisix"
    wait: yes
    wait_sleep: 10
    wait_timeout: 300
    wait_condition:
      type: Ready
      status: True

- name: Wait for APISiX Etcd to be ready
  kubernetes.core.k8s_info:
    kind: Pod
    namespace: "{{ k8s_namespace }}"
    kubeconfig: "{{ k8s_config }}"
    context: "{{ k8s_context }}"
    label_selectors: "app.kubernetes.io/name=etcd"
    wait: yes
    wait_sleep: 10
    wait_timeout: 300
    wait_condition:
      type: Ready
      status: True

- name: Add APIs to APISiX
  include_tasks:
    file: "{{ apis }}"
  loop:
    - 10_api_management_stack/retrieve_client_secret.yml
    - 10_api_management_stack/add_stellio_api.yml
    - 10_api_management_stack/add_masterportal_config_api.yml
    - 10_api_management_stack/add_geoserver_api.yml
    - 10_api_management_stack/add_geodata_citydb.yml
    - 10_api_management_stack/add_documentation_docusaurus.yml
    - 10_api_management_stack/add_frost_api.yml
  loop_control:
    loop_var: apis

# Please note: the space before the lookup of the file is on purpose. It is needed to avoid a bug in the lookup function. 
- name: Add APISIX Dashboard to Grafana
  kubernetes.core.k8s:
    kubeconfig: "{{ k8s_config }}"
    context: "{{ k8s_context }}"
    state: present
    definition:
      apiVersion: v1
      kind: ConfigMap
      metadata:
        name: apisix-dashboard
        namespace: "{{ inv_am.apisix.prometheus_namespace }}"
        labels:
          app.kubernetes.io/instance: "apisix"
          app.kubernetes.io/name: "apisix"
          grafana_dashboard: "1"
      data:
        apisix-dashboard-dashboard.json: " {{ lookup('file', 'templates/10_api_management/dashboards/apisix-dashboard.json') }}"
          


#  Setup velero backup
- name: "Create velero Backup schedule namespace {{ k8s_namespace }} "
  kubernetes.core.k8s:
    state: present
    kubeconfig: "{{ k8s_config }}"
    context: "{{ k8s_context }}"
    namespace: "{{ inv_op_stack.velero.ns_name }}"
    definition:
      apiVersion: velero.io/v1
      kind: Schedule
      metadata:
        name: "{{ k8s_namespace }}-backup"
      spec:
        schedule: 0 2 * * *
        template:
          defaultVolumesToRestic: true
          hooks: {}
          includedNamespaces:
            - '{{ k8s_namespace }}'
          metadata: {}
          snapshotVolumes: true
          storageLocation: "{{ inv_op_stack.velero.backup.location_name }}-backup"
          ttl: 720h0m0s
  when: inv_op_stack.velero.enable == "true"