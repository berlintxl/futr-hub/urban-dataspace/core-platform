---
- name: Set TimescaleDB internal endpoint
  ansible.builtin.set_fact:
    timescale_endpoint: "data-timescale.{{ inv_dm.ns_name }}"
  when: inv_mngd_db.quantumleap_postgres.enable == "false"

- name: Check if TimescaleDB internal endpoint exists
  kubernetes.core.k8s_info:
    kubeconfig: "{{ k8s_config }}"
    context: "{{ k8s_context }}"
    namespace: "{{ inv_dm.ns_name }}"
    kind: Service
    name: "data-timescale"
  register: timescale_endpoint_exists
  when: inv_mngd_db.quantumleap_postgres.enable == "false"

- name: Check for TimescaleDB internal endpoint failed
  fail:
    msg: "Internal endpoint data-timescale does not exist!"
  when: timescale_endpoint_exists['resources'][0] is undefined and inv_mngd_db.quantumleap_postgres.enable == "false"

- name: Set variables by managed TimescaleDB
  ansible.builtin.set_fact:
    quantumleap_postgres_user_secret: "{{ inv_mngd_db.quantumleap_postgres.user_k8s_secret }}"
    quantumleap_postgres_admin_secret: "{{ inv_mngd_db.quantumleap_postgres.admin_k8s_secret }}"
    timescale_endpoint: "{{ inv_mngd_db.quantumleap_postgres.db_address }}"
  when: inv_mngd_db.quantumleap_postgres.enable == "true"

- name: Set variables by self deployed TimescaleDB
  ansible.builtin.set_fact:
    quantumleap_postgres_user_secret: "{{ db_common.quantumleap_postgres.user.k8s_secret }}"
    quantumleap_postgres_admin_secret: "{{ db_common.quantumleap_postgres.admin.k8s_secret }}"
  when: inv_mngd_db.quantumleap_postgres.enable == "false"

- name: Check if QuantumLeap credentials exists in the cluster
  kubernetes.core.k8s_info:
    kubeconfig: "{{ k8s_config }}"
    context: "{{ k8s_context }}"
    namespace: "{{ inv_dm.ns_name }}"
    kind: Secret
    name: "{{ quantumleap_postgres_user_secret }}"
  register: quantumleap_credentials_exists
  no_log: '{{ ansible_debug.no_log }}'

- name: Check state of QuantumLeap credentials
  fail:
    msg: "Credentials {{ quantumleap_credentials_exists }} do not exist! Please, deploy TimescaleDB with user quantumleap."
  when: quantumleap_credentials_exists['resources'][0] is undefined

- name: Set QuantumLeap credentials to access TimescaleDB
  ansible.builtin.set_fact:
    quantumleap_credentials_username: "{{ quantumleap_credentials_exists['resources'][0]['data']['username'] | b64decode }}"
    quantumleap_credentials_password: "{{ quantumleap_credentials_exists['resources'][0]['data']['password'] | b64decode }}"
  when: quantumleap_credentials_exists['resources'][0] is defined
  no_log: '{{ ansible_debug.no_log }}'

- name: Check if TimescaleDB superuser credentials exists in the cluster
  kubernetes.core.k8s_info:
    kubeconfig: "{{ k8s_config }}"
    context: "{{ k8s_context }}"
    namespace: "{{ inv_dm.ns_name }}"
    kind: Secret
    name: "{{ quantumleap_postgres_admin_secret }}"
  register: postgres_credentials_exists
  no_log: '{{ ansible_debug.no_log }}'

- name: Check state of TimescaleDB superuser credentials
  fail:
    msg: "Credentials {{ postgres_credentials_exists }} do not exist! Please, deploy TimescaleDB."
  when: postgres_credentials_exists['resources'][0] is undefined

- name: Set TimescaleDB superuser credentials
  ansible.builtin.set_fact:
    postgres_credentials_username: "{{ postgres_credentials_exists['resources'][0]['data']['username'] | b64decode }}"
    postgres_credentials_password: "{{ postgres_credentials_exists['resources'][0]['data']['password'] | b64decode }}"
  when: postgres_credentials_exists['resources'][0] is defined
  no_log: '{{ ansible_debug.no_log }}'

- name: Deploy QuantumLeap
  no_log: '{{ ansible_debug.no_log }}'
  kubernetes.core.k8s:
    kubeconfig: "{{ k8s_config }}"
    context: "{{ k8s_context }}"
    namespace: "{{ k8s_namespace }}"
    definition: "{{ item }}"
    state: present
  loop:
    - "{{ lookup('template', 'templates/08_context_management/quantumleap.yml') | from_yaml_all | list }}"

- name: Wait till deployment of QuantumLeap is completed
  kubernetes.core.k8s_info:
    kubeconfig: "{{ k8s_config }}"
    context: "{{ k8s_context }}"
    namespace: "{{ k8s_namespace }}"
    kind: Pod
    label_selectors:
      - app=quantumleap
    wait: yes
    wait_sleep: 10
    wait_timeout: 360
