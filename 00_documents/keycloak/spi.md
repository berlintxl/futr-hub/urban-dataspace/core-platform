## Platform SPI Implementation

Keycloak offers multiple service provider interfaces to implement script-based features as plugins to keycloak. Read more in the official [documentation](https://www.keycloak.org/docs/latest/server_development/#_providers)

These interfaces are currently used to adjust the access_token and userinfo of the OIDC process to enable other components to decide wether a user is allowed to login, or perform certain actions.

The current implementation can be found [within this project](../../02_core_platform\templates\04_idm_keycloak\customization\udsp-keycloak-spis).

### Tenant Name Mapper
This mapper is used to provide information about the user assigned dataspaces (keycloak-groups). Upon login all groups, assigned to the user are scanned for 2 specific attributes:
```java
if (groupAttributes.containsKey("service") && groupAttributes.get("service").contains("true")) {
    if (groupAttributes.containsKey("tenant") && groupAttributes.get("tenant").contains("true")) {
        // If attributes 'service' and 'tenant' are set to true, add the group to the list of tenants
        tenants.add(Map.of("name", group.getName()));
    }
}
```

If these attributes set the dataspace is included in the access_token and therefore readable from applications which need to decide if a user may access portion of data. This is added by overriding the base method `setClaim`:

```java
    public class TenantNameMapper extends AbstractOIDCProtocolMapper
	implements OIDCAccessTokenMapper, OIDCIDTokenMapper, UserInfoTokenMapper

    ...

    @Override
    public void setClaim( ... ) {
        ...      
        // Add the "tenants" to the token
        token.getOtherClaims().put("tenants", tenants);
    }
```


### Organization Mapper
Similar to the tenant name mapper, this mapper iterates through all groups assigned to this user and scans for a specific structure.<br>
In this case the user needs to be in a subgroup which defines the role within an organization. This is also determined by evaluating the group attributes:

```java
for (GroupModel group : userGroups) {
    String orgName = null;
    if (group.getParent() != null) {
        // Check if the parent group has the "org" attribute
        Map<String, List<String>> parentAttributes = group.getParent().getAttributes();
        if (parentAttributes.containsKey("org") && parentAttributes.get("org").contains("true")) {
            // Use the parents group name
            orgName = group.getParent().getName()
        }
    }

    String groupName = group.getName();

    if ("admin".equals(groupName) || "editor".equals(groupName) || "member".equals(groupName)) {
        ckanRoles.add(Map.of("role", groupName, "org", orgName));
    }
}
```

Additonally if the user has the client-role ckanAdmin assigned, this role is added to the claim (this is used to decide wether or not a user shall have the rights and permissions of the SysAdmin):
```java

    // Check if the user has the client role "ckanAdmin"
    boolean hasCkanAdminRole = clientSessionCtx.getRolesStream()
            .map(RoleModel::getName)
            .anyMatch(roleName -> roleName.equals("ckanAdmin"));

    // Add "ckanAdmin" to ckanRoles if the user has the "ckanAdmin" client role
    if (hasCkanAdminRole) {
        ckanRoles.add("ckanAdmin");
    }

    // Add the "ckan_roles" to the token
    token.getOtherClaims().put("ckan_roles", ckanRoles);
```


### Build / Adjustments

These providers are build using maven. The default and suggested way of providing them to keycloak.<br>
In order to rebuild or integrate changes use `mvn install` at spis-folder level to create new target-jar files.


Copyright (c) 2023, Tegel Projekt GmbH. This work is licensed under the [European Union Public Licence (EUPL) v1.2](https://joinup.ec.europa.eu/collection/eupl/eupl-text-11-12).  
Author: Thomas Haarhoff, HYPERTEGRITY AG