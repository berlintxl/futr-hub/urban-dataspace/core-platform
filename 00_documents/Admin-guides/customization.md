## Customization

#### inventory
The inventory your main source for customizing the platform. This file and all of its content is described in the [Inventory Guide](inventory-guide.md).

### Customization Files
This section shows file based customization regarding specific components.
#### keycloak
With keycloak it is possible to use a custom theme, which is then applied for the realm and its clients.<br>
The files below show, where for example email-text and the background image can be customized.

- **/templates/keycloak/customization/email/messages_de.properties**<br>
This file contains the german templates for email content. it is placed inside the keycloak-instances during the deployment. <br>

- **/templates/keycloak/customization/custom-theme/theme/login/resources/img/keycloak-bg.png**<br>
This file contains the background image of the keycloak login screen. The file needs to be a png in order to get displayed and the target resolution should be 1920x1080. It is placed inside the keycloak-instances during the deployment. <br>

In order to use your changes a jar file, containing the ocp-custom folder, has to be created and an URL to it has to be provided within the inventory alongside the name of the theme:

```yaml
inv_idm:
    keycloak:
        theme: "<theme_name>" # Use "keycloak" for basic theme
        theme_url: "<theme_url>"
```

To create the jar file, you can for example use fastjar:
```bash
sudo apt install fastjar
jar cvf theme.jar .
```

### Dump and Restore Geodata
Once a new platform has been deployed, the need of transferring geodata from one instance to the new one may arise.<br>
In order to achieve this, a dump of the old data has to be created, the data has to be restored in the new database and maybe access rights have to be adjusted.

- Dump<br>
```bash
# USER most likely is the `postgres` user
pg_dumpall -h {DOMAIN} -p 31876 -U {USER} -W -c > db.out
```

- Restore<br>
```bash
# The database for geodata is a postgis, so the DATABASE_POD is something like geodata-postgis-{...}
kubectl exec -i {{DATABASE_POD}} -- /bin/bash -c "PGPASSWORD={USER_PASS} psql --username {USER} {DATABASE_NAME}" < db.out
```

- Adjust permissions
If the username of the database user has changed, the database-owner has to be set to the new one, by executing the following SQL command:<br>
```bash
ALTER DATABASE {DATABASE_NAME} OWNER TO {NEW_DATABASE_USER}
```

> **Note**: Restoring a dump also recreates users with the same username alongside with adjusting the password. The easiest way to fix this, is to log in to pgadmin and change usernames to the desired. For the system to work properly, the passwords for the two users `WEBGIS_POSTGRES_ADMIN` and `MAPSERVER_POSTGIS_USER` (found in the inventory), have to be set properly.
