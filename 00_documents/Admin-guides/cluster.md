# Cluster Installation
This Cluster Installation document will guide through the configuration and rollout of a base-cluster with needed components in order to enable availability and general stability.

If you already have a running k8s installation and want to install the platform components, please refer to the [Platform Installation](platform.md) guide.<br>


## Preface
Next to the platform and its components, we maintain a [repository](https://gitlab.com/urban-dataspace-platform/base-cluster) for a slim k8s cluster setup. <br>
The primary application of this repository is to provide platform environments used by developers and to create proof-of-concepts without having to rent production-ready infrastructure. 
This cluster setup utilizes a lightweight k8s distribution called [MicroK8s](https://microk8s.io/). This distribution has some benefits regarding the scope of this project, with the two main factors being:
- MicroK8s allows single-node clusters, which is the preferred environment for development. If more resources are needed, the usage of multi-node clusters is supported as well.
- MicroK8s does not require to start its own VM, which means the resource consumption regarding RAM, CPU and Disk Usage is less compared to distributions like Minikube.


## Usage
Since the application in the preface stated the general idea behind the repository and its focus: **The usage should not be extended to production.**<br>
If you are aiming to migrate from POC to production, or want to start directly in a production-ready environment, there is a general [guideline available](sizing.md) giving advice regarding sizing of different scenarios. If you have any questions feel free to create an issue, or contact us directly.


## Installation
The installation will be done by preparing a configuration file and running an ansible playbook.


### Requirements
A few requirements have to be met/prepared in order to create the installation process.
- A Linux server, with its IP and Root password.
- Any (Sub-)Domain pointing to the server (simplest would be to create a wildcard A record)
- A non-password [SSH key](https://www.ssh.com/academy/ssh/keygen) to be used on the server
- Any valid e-mail address for the certificate creation process with Let's Encrypt.

As for the machine executing the ansible playbook the following requirements have to be met.
- Any [git](https://git-scm.com/) client.
- Python >= 3.8
    - ansible.core >= 2.12
    - openshift 0.12.1
    - kubernetes 12.0.1 
    - ansible-lint >= 5.0 
    - docker >= 5.0 
- Helm
- Ansible Galaxy Module: kubernetes.core >= 2.2


### Configuration
Create a copy of the [default_inventory.yml](https://gitlab.com/urban-dataspace-platform/base-cluster/-/blob/master/01_base_cluster/default_inventory.yml) and fill out the variables. Some notes about the configuration:
- Some fields are filled with preconfigured values which are suitable for the majority of installations. 
- `DOMAIN`: The target (sub-)domain of the accessible platform. i.e. dev.example.com
- `kubeconfig_file`: Once MicroK8s is installed the main kubeconfig-file, used for example with `kubectl` is created and stored. This variable reflects the name of the created file.
- `hosts`: This section is to configure the machines to be provisioned. Fill only the section `k8s-master` if the cluster shall run as single-node. Fill them all, if 3 nodes shall be used.
- `platform_sskey_*`: path and name of the ssh-key to be used to access the cluster after installation. 
- `cert_manager_le_mail`: Email used during Let's Encrypt process and to be notified for certificate renewal.

Additionally it is possible to deploy a private GitLab-CE instance:
- `gitlab_domain`: The domain GitLab shall use. i.e. GitLab.\<DOMAIN\>

Also it is possible to deploy dedicated GitLab runners:
- `RUNNER_REGISTRATION_TOKEN`: This token a GitLab Admin can provide once GitLab is deployed.

>If you need to setup the k8s-cluster behind a proxy, you will need to set the prepared section for Proxy environment variables in order for the deployed components to be reachable. You may want/need to coordinate this with your hoster.

### Rollout
1. Checkout the base-cluster [repository](https://gitlab.com/urban-dataspace-platform/base-cluster):
```bash
git clone https://gitlab.com/urban-dataspace-platform/base-cluster
cd base-cluster
```
2. Place the created `inventory` somewhere accessible.
3. Run the playbook

Single-node: 
```bash
ansible-playbook -l k8s-master,localhost -i inventory 01_base_cluster/playbook.yml --tags single_node
```
Multi-Node
```bash
ansible-playbook -l k8s-master,k8s-node-1,k8s-node-2,localhost -i inventory 01_base_cluster/playbook.yml --tags multi_node
```
