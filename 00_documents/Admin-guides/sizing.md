# Platform Requirements & Sizing for the core data platform

The following document describes different sizing scenarios for the core platform components, that can be used to get a first guess of the needed hardware software for the running the data platform.

Important to consider is the fact, that the recommendations regarding sizing can only be a starting point for the right sizing of the platform. The concrete figures depend on use case on the platform, the generated load from the use cases and the hardware architecture & generation on which the platform in run.

# Requirements Overview

## Hosting Requirements

### Basic general requirements

The data platform has in general no direct dependencies to any hardware specific functionality. The only requirement is a x86 64 Bit CPU Architecture, because now, the document is written, all container images that are used, are available for x86 CPUs. Many of them are available for ARM based CPUs, too - but not all. This may change in the future.

The platform can run as a sandbox environment for evaluation scenarios on a single host with direct filesystem usage of the host system. This is only suitable for feature testing, not for running a scalable production platform.

The minimum number of host for a full kubernetes environment is three. Three and more nodes can provide a high available environment for running the platform. This requirement is defined by Kubernetes itself, because of the need, that etcd and other control plane services need at least three nodes, to run in an HA-scenario. 

In a managed kubernetes scenario, the control plane and etcds can be provided as a shared services by the provider. In this case, only worker nodes must be provided, the recommendation is three or more, too. 

### Hardware Requirements

The following chapters define hardware requirements for different scenarios of platform usage.

#### Sandbox (1 Node Cluster)
For installing and running a sandbox, the platform can be used with the provided MicroK8s Setup as a one node Kubernetes Cluster. All Replica Numbers can be reduced to 1.

In this case, we recommend the following hardware specs:
* 8-10 vCPUs (can be shared)
* min. 32 GB RAM
* min. 400 GB SSD Storage

#### Minimum (3 Node Cluster)
For installing and running the minimum platform, the provided MicroK8s Setup with three or more nodes can be used. Any other managed Kubernetes Solution will work, too. We recommend starting with three nodes and add more when needed.

In this case, we recommend the following hardware specs per Node:
* 8-10 vCPUs (can be shared, dedicated preferred)
* min. 32 GB RAM
* min. 400 GB SSD Storage

Additionally, a network storage of at least 500GB with SSD Performance should be provided. The Network Storage should support Kubernetes ReadWriteOnce and ReadWriteMany Access. The Node internal Storage will be used for System Storage.
The Amount of needed Network Storage depends on the implemented use cases. So, this must be aligned on a project base.

#### Standard (3 Node Cluster)
For installing and running the standard platform, the provided MicroK8s Setup with four or more nodes can be used. Any other managed Kubernetes Solution will work, too. We recommend starting with four nodes and add more when needed. A managed Kubernetes Solution would be preferred.

In this case, we recommend the following hardware specs per Node:
* min. 12 dedicated Cores 
* min. 64 GB RAM
* min. 400 GB SSD Storage

Additionally, a network storage of at least 500 GB with SSD Performance should be provided. More can be required depending on use cases. The Network Storage should support Kubernetes ReadWriteOnce and ReadWriteMany Access. The Node internal Storage will be used for System Storage
The Amount of needed Network Storage depends on the implemented use cases. So, this has to be aligned on a project base.

#### Outlook
With each use case additional resource can be required. Due to the fact that most use cases generate peak resource request but use over time only a part of the peak resources, there should be a resource efficient way to provide more resources.

##### Alternative 1: Overprovision on Kubernetes Level
In this case the owner of the system accepts that if all components run on peak resource requirements, there will not be enough resources in the cluster and the response behaviour will be slow for this time. But the likelihood, that this case will come up is very slow and acceptable. In this case, a static resource provisioning can work. 

##### Alternative 2: Dynamic extension of the cluster on demand
Based on technology like the Vertical Pod Autoscaler and other mechanisms, the cluster can allocate additional resources on form of additional or bigger nodes on demand. This must be configured individually per installation. 
The precondition for this is a cluster management, that supports dynamic provisioning an a underlying cloud infrastructure, that supports providing additional resources on demand. This can be an OpenStack Environment or an API managed Hosting Environment.


# Managed Services

Beside the described hardware requirements, the platform can be used in different usage scenarios and defines depending on the usage scenario different software requirements.

The scenarios and their requirements are described in the following chapters.

## Alternative 1: MicroK8s Base Cluster on VMs

### Typical Usage
This scenario defines the lowest software requirements. Based on the published repositories the platform can be installed including the full kubernetes stack. In this case, only virtual machines for the kubernetes notes are needed. 

This installation mode is recommended for Proof-of-concept installations or requirements with low automatic scaling possibilities.

In this scenario, the highest effort for the operations team is created.

### Requirements

We recommend a Debian or Ubuntu based Virtual machine setup, preferred with the most current LTS Versions of the operating systems. 

The included Kubernetes Setup uses microK8s to create a Cluster. In simple usage scenarios, a single node setup can be used. We recommend three node setup with the integrated provisioning of cluster storage based on OpenEBS. 

For one node setups, we recommend on node of the category of the described Standard Setup for Hardware above.

The create a simple High Availability Setup on Kubernetes Level, a failover IP can be used and managed on OS Level. To run the provided services with the integrated Let's Encrypt Certificate a DNS Wildcard Domain per Stage is needed. 

To optimize the setup on public hosting environments, a VLAN between the cluster nodes is recommended to run the cluster internal traffic on this. 

### Restrictions

The Scenario does not support a dynamic scaling of the cluster nodes. Only a static extension of the nodes can be integrated, so if the provisioned hardware is completely used, an additional node must be added manually.

## Alternative 2: Managed Hosting of K8s 

### Typical Usage

This scenario should be used, if the lowest dependency on the cluster management provider should be created.

In this case a vanilla kubernetes cluster is needed. If a full dedicated cluster should be used, we recommend as described above at least three nodes for running the kubernetes control plane and the workload. 

The second alternative is a managed cluster, where the control plane is a shared service of the hosting provider and only worker nodes are booked. In this case at least two nodes should be used, with the corresponding level of resources.

This installation is recommended for any productive setup.

### Requirements

The kubernetes cluster should use the most current version of Kubernetes 1.21 or above. 

Provided services must be:
* Ingress
* Distributed File System (CEPH, OpenEBS, Longhorn, ...) with ReadWriteOnce AND ReadWriteMany Support

Optional Services, that are recommended to be provided:
* Monitoring & Logging Stack
* Cert-Manager
* Local File System Provisioner for Database Instances (Replication is done by the Database)

The cluster should be reachable over one external IP (Floating IP or Load Balancer) on which the DNS-Names as Wildcards can be configured.

### Restrictions

Dependency in the provided software versions on the provider.

## Alternative 3: Managed Hosting of K8s + managed Operators

### Typical Usage

This scenario should be used, to reduce the own operating effort for running the cluster.

In this case a vanilla kubernetes cluster is needed. If a full dedicated cluster should be used, we recommend as described above at least three nodes for running the kubernetes control plane and the workload. 

The second alternative is a managed cluster, where the control plane is a shared service of the hosting provider and only worker nodes are booked. In this case at least two nodes should be used, with the corresponding level of resources.

In addition to this, the kubernetes operators, the platform uses, are run by the provider.

This installation is recommended for any productive setup.

### Requirements

The kubernetes cluster should use the most current version of Kubernetes 1.21 or above. 

Provided services must be:
* Ingress
* Distributed File System (CEPH, OpenEBS, Longhorn, ...) with ReadWriteOnce AND ReadWriteMany Support

Required Operators:
* Keycloak Operator
* Zalando Postgres Operator
* Percona MySQL Operator
* Percona Mongo Operator
* Minio Operator

Optional Services, that are recommended to be provided:
* Monitoring & Logging Stack
* Cert-Manager
* Local File System Provisioner for Database Instances (Replication is done by the Database)

The cluster should be reachable over one external IP (Floating IP or Load Balancer) on which the DNS-Names as Wildcards can be configured.

### Restrictions

Dependency in the provided software versions on the provider.

# Alternative 4: Managed Hosting of K8s + managed Operators + managed Databases

### Typical Usage

This scenario should be used, to reduce the own operating effort for running the cluster.

In this case a vanilla kubernetes cluster is needed. If a full dedicated cluster should be used, we recommend as described above at least three nodes for running the kubernetes control plane and the workload. 

The second alternative is a managed cluster, where the control plane is a shared service of the hosting provider and only worker nodes are booked. In this case at least two nodes should be used, with the corresponding level of resources.

In addition to this, the kubernetes operators, the platform uses, are run by the provider. The services based on these operators are managed by the provider.

This installation is recommended for any productive setup.

### Requirements

The kubernetes cluster should use the most current version of Kubernetes 1.21 or above. 

Provided services must be:
* Ingress
* Distributed File System (CEPH, OpenEBS, Longhorn, ...) with ReadWriteOnce AND ReadWriteMany Support

Required Operators including the managed instances:
* Keycloak Operator
* Zalando Postgres Operator
* Percona MySQL Operator
* Percona Mongo Operator
* Minio Operator

Optional Services, that are recommended to be provided:
* Monitoring & Logging Stack
* Cert-Manager
* Local File System Provisioner for Database Instances (Replication is done by the Database)

The cluster should be reachable over one external IP (Floating IP or Load Balancer) on which the DNS-Names as Wildcards can be configured.

### Restrictions

Dependency in the provided software versions on the provider.

The Dependency is much higher. For a high dynamic phase of platform and use case development, we do not recommend this scenario. In Alternative 3 there is more flexibility to test developments.

## Software Versions recommended

The software versions, that are used and needed by the platform are pinned in the must current release of the platform repositories. Due to the ongoing development these versions change.

In general, we recommend to be up-to-date with the current versions of the respective components, to avoid and bugs and security problems of older versions. 

## Stages

For Proof-of-Concept Setups only one Sandbox Installation is recommended. 

For Implementation Projects, we recommend at least two stages for the development process:

* Staging: For Integration and Testing Work
* Production: For the productive usage

Additional central DEV-Stages can be used for early testing but are not needed in each case.

The development work is normally done on decentral DEV Instances per Developer.

## Final Recommendation

In Total we recommend a hardware sizing, that follows the trends of Green IT. This means an overprovisioning should be avoided. In most hosting scenarios, additional resources can be book on short notice. The static upscaling can be archived by this. A good starting point, when use cases are not clear at the beginning is the **Minimum** Sizing.

Best way to scale is the dynamic scaling, but this depends on the hosting provider capabilities.

Regarding the Managed Services we recommend deciding between Alternative 2 and 3. Both scenarios provide together with the capabilities of the platform the best flexibility in changing the hosting provider, if needed. The important services are self-managed. The slightly higher effort in managing the services is acceptable to avoid a higher dependency.


