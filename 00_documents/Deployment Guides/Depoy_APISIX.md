# Deploy APISIX

This document describes how to deploy APISIX on the platform using the Ansible Playbook.

The following components will be deployed:

- APISIX
  - The Admin API
- APISIX Dashboard
  - The Admin UI
- APISIX Ingress Controller
  - The Ingress Controller for APISIX
- etcd
  - The database for APISIX

## Step 1: Add APISIX to inventory 
- Copy and paste the following into the inventory file under `inv_am`: 
```yaml
inv_am:
    apisix:
        enable: "true"
        ns_create: "true"
        ns_name: "{{ ENVIRONMENT }}-api-management-stack"
        ns_kubeconfig: "{{ kubeconfig_file }}"
        api_credentials:
            admin_role: ""
            viewer_role: ""
        dashboard:
            jwt_secret: ""
            admin:  
                username: "admin@{{ DOMAIN }}"
                password: ""
```

## Step 2: Add secrets:
- Secrets will have to be generated for `admin_role`, `viewer_role`, `jwt_secret`, and `password`. Make sure they are all different.

## Step 3: Add replica variables to inventory
- Copy and paste the following into the inventory file under `inv_replicas`:
```yaml
api_mgmt:
  apisix: 1
  etcd: 1
```
- Set the replicas according to the needs of the deployment.

## Step 4: Run the playbook
- Run the playbook with the following command and tag:
```bash
ansible-playbook -l localhost -i inventory.yml 02_core_platform/full_install.yml --tags "api_mgmt"
```